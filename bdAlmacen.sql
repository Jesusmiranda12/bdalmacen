create database almacen;
use almacen;
#region Tablas#
create table Edificio(
IDEdificio int primary key,
NombreEdificio varchar(50) 
);

create table categoria(
idCategoria varchar(50) primary key ,
nombreCategoria varchar(50)
);

create table grupo(
idGrupo varchar(50) primary key,
semestreGrupo int,
aulaGrupo varchar(15),
carreraGrupo varchar(50)
);

create table alumno(
numerocontrolAlumno int primary key,
nombreAlumno varchar(50),
apellidopaternoAlumno varchar(50),
apellidomaternoAlumno varchar(50),
telefonoAlumno varchar(11),
correoAlumno varchar(50),
codigocredencialAlumno varchar(20),
fkgrupo varchar(50),
foreign key(fkgrupo) references grupo(idGrupo) on delete cascade on update cascade
);

create table maestro(
claveMaestro varchar(50) primary key,
nombreMaestro varchar(50),
apellidopaternoMaestro varchar(50),
apellidomaternoMaestro varchar(50)
);

create table color(
idColor int primary key auto_increment,
nombreColor varchar(50),
significadoColor varchar(50)
);

create table laboratorio(
nombreLaboratorio varchar(50) primary key ,
fkidEdificio int,
fkcolor int,
foreign key(fkcolor) references color(idColor),
foreign key(fkidEdificio) references Edificio(IDEdificio) on delete cascade on update cascade
);

create table materia(
claveMateria varchar(50) primary key,
nombreMateria varchar(50)
);

create table materias_grupos(
idMG int primary key,
fkmateria varchar(50),
fkgrupo varchar(50),
foreign key(fkmateria) references materia(claveMateria),
foreign key(fkgrupo) references grupo(idGrupo) on delete cascade on update cascade
);

create table Prestamocable(
idPrestamocable int primary key,
tipoPrestamocable varchar(50),
numerocables int,
fechaprestamoPrestamocable varchar(50),
fechaentregaPrestamocable varchar(50),
fkgrupo varchar(50),
foreign key(fkgrupo) references grupo(idGrupo) on delete cascade on update cascade
);

create table Prestamoproyector(
idPrestamoproyector int primary key,
fechaprestamoPrestamocable varchar(50),
fechaentregaPrestamocable varchar(50),
horainicioProyector varchar(15),
horafinalProyector varchar(15),
fkgrupo varchar(50),
foreign key(fkgrupo) references grupo(idGrupo) on delete cascade on update cascade
);

create table herramienta(
codigoHerramienta varchar(50) primary key,
conceptoHerramienta varchar(50),
etiquetaHerramienta varchar(50),
marcaHerramienta varchar(50),
modeloHerramienta varchar(50),
estadoHerramienta varchar(50),
ubicacionHerramienta varchar(50),
activoResguardoHerramienta varchar(50),
plantaHerramienta varchar(50),
observacionesHerramienta varchar(50),
fkEdificio int,
fkcategoria varchar(50),
foreign key(fkcategoria) references categoria(idCategoria),
foreign key(fkEdificio) references Edificio(IDEdificio) on delete cascade on update cascade
);

create table almacenista(
idAlmacenista int primary key auto_increment,
nombreAlmacenista varchar(50),
apellidopaternoAlmacenista varchar(50),
apellidomaternoAlmacenista varchar(50),
correoAlmacenista varchar(50),
cargoAlmacenista varchar(50)
);

create table prestamo_laboratorio(
idPrestamolaboratorio int primary key auto_increment,
fechaPrestamolaboratorio date,
horainicioPrestamolaboratorio varchar(15),
horafinalPrestamolaboratorio varchar(15),
fklaboratorio varchar(50),
fkmaestro varchar(50),
foreign key(fklaboratorio) references laboratorio(nombreLaboratorio),
foreign key(fkmaestro) references maestro(claveMaestro) on delete cascade on update cascade
);

create table prestamo_herramienta(
idprestamoHerramienta int primary key auto_increment,
fechaPrestamoherrmienta varchar(50),
fechadevolucionPrestamoherramienta varchar(50),
statusPrestamoherramienta varchar(50),
cantidadPrestamoherramienta int,
fkherramienta varchar(50),
fkalumno int,
fkmateria varchar(50),
foreign key(fkherramienta) references herramienta(codigoHerramienta),
foreign key(fkalumno) references alumno(numerocontrolAlumno),
foreign key(fkmateria) references materia(claveMateria) on delete cascade on update cascade
);

create table usuarios(
idUsuario int primary key auto_increment,
usuario varchar(200),
contrase�a varchar(50),
nombre varchar(100),
apellidos varchar(200),
email varchar(100),
tipodecuenta varchar(100));
#end region
#region Procedures
#region procedures edificio
/*PROCEDURES DE EDIFICIO*/

create procedure p_insertarEdificio(in _ID int,in _nombreEdificio varchar(50))
begin
insert into Edificio values(_ID, _nombreEdificio);
end;

create procedure p_eliminarEdificio(in _ID int)
begin
delete from Edificio where IDEdificio = _ID;
end;

create procedure p_actualizarEdificio(in _ID int,in _nombreEdificio varchar(50))
begin
update Edificio set  NombreEdificio = _nombreEdificio where IDEdificio = _ID;
end;
#end region
#region procedures categoria
/*PROCEDURES DE CATEGORIA*/
drop procedure p_insertarCategoria;
create procedure p_insertarCategoria(in _idCategoria varchar(50), _nombreCategoria varchar(50))
begin
insert into categoria values (_idCategoria, _nombreCategoria);
end;

drop procedure p_eliminarCategoria;
create procedure p_eliminarCategoria(in _idCategoria varchar(50))
begin
delete from categoria where idCategoria  = _idCategoria ;
end;

drop procedure p_actualizarCategoria;
create procedure p_actualizarCategoria(in _idCategoria varchar(50), _nombreCategoria varchar(50))
begin
update categoria set idCategoria = _idCategoria, nombreCategoria = _nombreCategoria  where idCategoria = _idCategoria ;
end;
#end region;
#region Procedures Grupo
create procedure p_insertarGrupo(
in p_idGrupo varchar(50),
in p_semestreGrupo int,
in p_aulaGrupo varchar(15),
in p_carreraGrupo varchar(50))
BEGIN
    INSERT INTO grupo VALUES(p_idGrupo, p_semestreGrupo_m, p_aulaGrupo, p_carreraGrupo);
END;

create procedure p_modificarGrupo(
in p_idGrupo varchar(50),
in p_semestreGrupo int,
in p_aulaGrupo varchar(15),
in p_carreraGrupo varchar(50))
BEGIN
UPDATE grupo SET
semestreGrupo = p_semestreGrupo,
aulaGrupo = p_aulaGrupo,
carreraGrupo = p_carreraGrupo
WHERE idGrupo = p_idGrupo;
END;

create procedure p_eliminarGrupo(
in p_idGrupo varchar(50))
BEGIN
DELETE FROM grupo WHERE idGrupo = p_idGrupo;
END;
#end region 
#region Procedures Alumno
create procedure p_insertarAlumno(
in p_numerocontrolAlumno int,
in p_nombreAlumno varchar (50),
in p_apellidopaternoAlumno varchar(50),
in p_apellidomaternoAlumno varchar (50),
in p_telefonoAlumno varchar(11),
in p_correoAlumno varchar (50),
in p_codigocredencialAlumno varchar (20),
in p_fkgrupo varchar (50))
BEGIN
    INSERT INTO alumno VALUES(p_numerocontrolAlumno, p_nombreAlumno, p_apellidopaternoAlumno, p_apellidomaternoAlumno,
    p_telefonoAlumno,p_correoAlumno, p_codigocredencialAlumno, p_fkgrupo);
END;

create procedure p_modificarAlumno(
in p_numerocontrolAlumno int,
in p_nombreAlumno varchar (50),
in p_apellidopaternoAlumno varchar(50),
in p_apellidomaternoAlumno varchar (50),
in p_telefonoAlumno varchar(11),
in p_correoAlumno varchar (50),
in p_codigocredencialAlumno varchar (20),
in p_fkgrupo varchar (50))
BEGIN
UPDATE alumno SET
nombreAlumno = p_nombreAlumno, 
apellidopaternoAlumno = p_apellidopaternoAlumno,
apellidomaternoAlumno = p_apellidomaternoAlumno,
telefonoAlumno = p_telefonoAlumno,
correoAlumno = p_correoAlumno,
codigocredencialAlumno = p_codigocredencialAlumno,
fkgrupo = p_fkgrupo
WHERE numerocontrolAlumno = p_numerocontrolAlumno;
END;

create procedure p_eliminarAlumno(
in p_numerocontrolAlumno int )
BEGIN
DELETE FROM Alumno where numerocontrolAlumno = p_numerocontrolAlumno;
END;
#end region 
#region procedures de laboratorios
create procedure p_insertarLaboratorio(in _nombre varchar(5), _fkidEdificio int, _fkcolor int)
begin
insert into laboratorio values(_nombre,_fkidEdificio,_fkcolor);
end;
create procedure p_eliminarLaboratorio(in _nombre varchar(50))
begin
delete from laboratorio where nombreLaboratorio = _nombre;
end;
create procedure p_modificarLaboratorio(in _nombre varchar(50), in _fkidEdificio int, in _fkcolor int,in _nombreLaboratorio varchar(50))
begin
update laboratorio set nombreLaboratorio = _nombre,fkidEdificio = _fkidEdificio,fkcolor = _fkcolor where nombreLaboratorio = _nombreLaboratorio;
end;
#end region
#region procedures de materia
create procedure p_insertarMateria(in _claveMateria varchar(50), _nombreMateria varchar(50))
begin
insert into materia values(_claveMateria,_nombreMateria);
end;
create procedure p_eliminarMateria(in _clave int)
begin
delete from materia where claveMateria=_clave;
end;
create procedure p_modificarMateria(in _clave varchar(50), in _nombreMateria varchar(50), in _claveMateria varchar(50))
begin
update materia set claveMateria = _clave, nombreMateria = _nombreMateria where claveMateria = _claveMateria;
end;
#end region
#region Procedures Maestros
create procedure p_maestro_registrar(
in p_clave_maestro varchar(50),
in p_nombre_m varchar(50),
in p_apellidop varchar(50),
in p_apellidom varchar(50))
BEGIN
    INSERT INTO maestro VALUES(p_clave_maestro, p_nombre_m, p_apellidop, p_apellidom);
 END;

create procedure p_maestro_modificar(
in p_clave_maestro varchar(50),
in p_nombre_m varchar(50),
in p_apellidop varchar(50),
in p_apellidom varchar(50))
BEGIN
UPDATE maestro SET
claveMaestro = p_clave_maestro,
nombreMaestro = p_nombre_m,
apellidoPaternoMaestro = p_apellidop,
apellidoMaternoMaestro = p_apellidom
WHERE claveMaestro = p_clave_maestro;
END;

create procedure p_maestro_eliminar(
in p_clave_maestro varchar(50))
BEGIN
DELETE FROM maestro WHERE claveMaestro = p_clave_maestro;
END;


#end region 
#region Procedures Color
create procedure p_color_registrar(
in p_id int,
in p_color varchar(50),
in p_significado varchar(50))
BEGIN
    INSERT INTO color VALUES(p_id, p_color, p_significado);
END;

create procedure p_color_modificar(
in p_id int,
in p_color varchar(50),
in p_significado varchar(50))
BEGIN
UPDATE color SET
	nombreColor = p_color,
	significadoColor = p_significado
    WHERE idColor = p_id;
END;

create procedure p_color_eliminar(in p_id int)
BEGIN
DELETE FROM color WHERE idColor = p_id;
END;





#end region
#region MateriasGrupos
create procedure p_insertarMateriasGrupos(in _idMG int, in _fkmateria varchar(50), in _fkgrupo varchar(50))
begin 
insert into materias_grupos values(_idMG, _fkmateria , _fkgrupo);
end;
create procedure p_eliminarMateriasGrupos(in _idMG int)
begin
delete from materias_grupos where idMG = _idMG;
end;
create procedure p_actualizarMateriasGrupos(in _fkmateria varchar(50), in _fkgrupo varchar(50), in _idMG int)
begin
update materias_grupos set fkmateria = _fkmateria, fkgrupo = _fkgrupo where idMG = _idMG;
end;
#end region
#region Prestamocable
create procedure p_insertarPrestamocable(in _idPrestamocable int, in _tipoPrestamocable varchar(50), in _numerocables int, 
in _fechaprestamoPrestamocable varchar(50), in _fechaentregaPrestamocable varchar(50), in _fkgrupo varchar(50))
begin 
insert into prestamocable values(_idPrestamocable, _tipoPrestamocable , _numerocables,
_fechaprestamoPrestamocable, _fechaentregaPrestamoclabe, _fkgrupo);
end;
create procedure p_eliminarPrestamocable(in _idPrestamocable int)
begin
delete from prestamocable where idPrestamocable = _idPrestamocable;
end;
create procedure p_actualizarPrestamocable(in _tipoPrestamocable varchar(50), in _numerocables int, 
in _fechaprestamoPrestamocable varchar(50), in _fechaentregaPrestamocable varchar(50), in _fkgrupo varchar(50),
in _idPrestamocable int)
begin
update prestamocable set idPrestamocable = _idPrestamocable, tipoPrestamocable = _tipoPrestamocable , numerocables = _numerocables,
fechaprestamoPrestamocable = _fechaprestamoPrestamocable, fechaentregaPrestamocable = _fechaentregaPrestamoclabe, fkgrupo = _fkgrupo where idPrestamocable = _idPrestamocable;
end;
#end region
#region PrestamoProyector
create procedure p_InsertarPrestamoProyector(
in _idPrestamoproyector int ,
in _fechaprestamoPrestamocable varchar(50),
in _fechaentregaPrestamocable varchar(50),
in _horainicioProyector varchar(15),
in _horafinalProyector varchar(15),
in _fkgrupo varchar(50))

begin  
     insert into Prestamoproyector values(
     _idPrestamoproyector,
     _fechaprestamoPrestamocable,
     _fechaentregaPrestamocable,
     _horainicioProyector,
     _horafinalProyector,
     _fkgrupo);        
end;




create procedure p_BorrarPrestamoProyector(
in _idPrestamoproyector int)

begin 
     delete from Prestamoproyector where idPrestamoproyector = _idPrestamoproyector;
end;




create procedure p_ModificarPrestamoProyector(
in _idPrestamoproyector int ,
in _fechaprestamoPrestamocable varchar(50),
in _fechaentregaPrestamocable varchar(50),
in _horainicioProyector varchar(15),
in _horafinalProyector varchar(15),
in _fkgrupo varchar(50))

begin  
    update Prestamoproyector set
     fechaprestamoPrestamocable =_fechaprestamoPrestamocable,
     fechaentregaPrestamocable=_fechaentregaPrestamocable,
     horainicioProyector=_horainicioProyector,
     horafinalProyector=_horafinalProyector,
     fkgrupo = _fkgrupo where idPrestamoproyector = _idPrestamoproyector;        
end;

#end region               
#region Procedures de Herramientas
create procedure p_InsertarHerramienta(
in _codigoHerramienta varchar(50) ,
in _conceptoHerramiento varchar(50),
in _etiquetaHerramienta varchar(50),
in _marcaHerramienta varchar(50),
in _modeloHerramienta varchar(50),                 /*  <--- Parametros de entrada */
in _estadoHerramienta varchar(50),
in _ubicacionHerramienta varchar(50),
in _activoResguardoHerramienta varchar(50),
in _plantaHerramienta varchar(50),
in _observacionesHerramienta varchar(50),
in _fkEdificio varchar(50),
in _fkcategoria varchar(50))

begin 
insert into herramienta values(
 _codigoHerramienta,
 _conceptoHerramiento,
 _etiquetaHerramienta,
 _marcaHerramienta,
 _modeloHerramienta,
 _estadoHerramienta ,
 _ubicacionHerramienta,
 _activoResguardoHerramienta,
 _plantaHerramienta,
 _observacionesHerramienta,
 _fkEdificio ,
 _fkcategoria );
 end;
 

 create procedure p_BorrarHerramienta( in _codigoHerramienta varchar(50))
 begin
 delete from herramienta where codigoHerramienta=_codigoHerramienta;
 end;
 
 
create procedure p_ModificarHerramienta(
in _codigoHerramienta varchar(50) ,
in _conceptoHerramiento varchar(50),
in _etiquetaHerramienta varchar(50),
in _marcaHerramienta varchar(50),
in _modeloHerramienta varchar(50),                 /*  <--- Parametros de entrada */
in _estadoHerramienta varchar(50),
in _ubicacionHerramienta varchar(50),
in _activoResguardoHerramienta varchar(50),
in _plantaHerramienta varchar(50),
in _observacionesHerramienta varchar(50),
in _fkEdificio varchar(50),
in _fkcategoria varchar(50))

begin 
update herramienta set 
 conceptoHerramiento=_conceptoHerramiento,
 etiquetaHerramienta=_etiquetaHerramienta,
 marcaHerramienta=_marcaHerramienta,
 modeloHerramienta =_modeloHerramienta,
 estadoHerramienta=_estadoHerramienta ,
 ubicacionHerramienta=_ubicacionHerramienta,
 activoResguardoHerramienta=_activoResguardoHerramienta,
 plantaHerramienta=_plantaHerramienta,
 observacionesHerramienta=_observacionesHerramienta,
 fkEdificio=_fkEdificio ,
 fkcategoria=_fkcategoria 
 where codigoHerramienta=_codigoHerramienta;
 end;
#end region      
#region Prestamos de Herramientas
/*INSERT*/
create procedure p_insertarPrestamoHerramienta(
in _idprestamoHerramienta int, 
in _fechaPrestamoherrmienta varchar (50),
in _fechadevolucionPrestamoherramienta varchar(50),
in _statusPrestamoherramienta varchar(50),
in _cantidadPrestamoherramienta int,
in _fkherramienta varchar (50),
in _fkalumno int,
in _fkmateria varchar (50)
)
begin
insert into prestamo_herramienta values(
_idprestamoHerramienta,
_fechaPrestamoherrmienta,
_fechadevolucionPrestamoherramienta,
_statusPrestamoherramienta,
_cantidadPrestamoherramienta,
_fkherramienta ,
_fkalumno,
_fkmateria);

end;



/*UPDATE*/

create procedure p_actualizarPrestamoHerramienta(
in _idprestamoHerramienta int, 
in _fechaPrestamoherrmienta varchar (50),
in _fechadevolucionPrestamoherramienta varchar(50),
in _statusPrestamoherramienta varchar(50),
in _cantidadPrestamoherramienta int,
in _fkherramienta varchar (50),
in _fkalumno int,
in _fkmateria varchar (50))

begin
update prestamo_herramienta set 
fechaPrestamoherrmienta =_fechaPrestamoherrmienta,
fechadevolucionPrestamoherramienta = _fechadevolucionPrestamoherramienta,
statusPrestamoherramienta = _statusPrestamoherramienta,
cantidadPrestamoherramienta = _cantidadPrestamoherramienta,
fkherramienta = _fkherramienta,
fkalumno = _fkalumno,
fkmateria = _fkmateria

where idprestamoHerramienta = _idprestamoHerramienta;

end;
 
  /*DELETE*/
  
  create procedure p_eliminarPrestamoHerramienta(in idprestamoHerramienta int )
begin
delete from prestamo_herramienta where idprestamoHerramienta = _idprestamoHerramienta;
end;

#end region 
#region almacenista
create procedure p_insertarAlmacenista(
in _idAlmacenista int,
in _nombreAlmacenista varchar(50),
in _apellidopaternoAlmacenista varchar(50),
in _apellidomaternoAlmacenista varchar(50),
in _correoAlmacenista varchar(50),
in _cargoAlmacenista varchar(50)
)
begin
insert into almacenista values(null ,_nombreAlmacenista,_apellidopaternoAlmacenista,_apellidomaternoAlmacenista,_correoAlmacenista,_cargoAlmacenista);
end;

create procedure p_eliminarAlmacenista(in _idAlmacenista int)
begin
delete from almacenista where idAlmacenista = _idAlmacenista;
end;

create procedure p_actualizarAlmacenista(
in _idAlmacenista int,
in _nombreAlmacenista varchar(50),
in _apellidopaternoAlmacenista varchar(50),
in _apellidomaternoAlmacenista varchar(50),
in _correoAlmacenista varchar(50),
in _cargoAlmacenista varchar(50)
)
begin
update almacenista set nombreAlmacenista = _nombreAlmacenista,apellidopaternoAlmacenista=_apellidopaternoAlmacenista,apellidomaternoAlmacenista = _apellidomaternoAlmacenista,correoAlmacenista = _correoAlmacenista,cargoAlmacenista = _cargoAlmacenista where idAlmacenista=_idAlmacenista;
end;
#end region             
#region Prestamolaboratorio
create procedure p_insertarPrestamoLab(
in _idPrestamolaboratorio int (11), 
in _fechaPrestamolaboratorio date,
in _horainicioPrestamolaboratorio varchar(15),
in _horafinalPrestamolaboratorio varchar(15),
in _fklaboratorio varchar(50),
in _fkmaestro varchar (50)
)
begin
insert into prestamo_laboratorio values(_idPrestamolaboratorio,_fechaPrestamolaboratorio,_horainicioPrestamolaboratorio,
_horafinalPrestamolaboratorio,_fklaboratorio,_fkmaestro);
end;


create procedure p_actualizarPrestamoLab(
in _idPrestamolaboratorio int (11), 
in _fechaPrestamolaboratorio date,
in _horainicioPrestamolaboratorio varchar(15),
in _horafinalPrestamolaboratorio varchar(15),
in _fklaboratorio varchar(50),
in _fkmaestro varchar (50))
begin
update prestamo_laboratorio set 

fechaPrestamolaboratorio =_fechaPrestamolaboratorio,
horainicioPrestamolaboratorio = _horainicioPrestamolaboratorio,
horafinalPrestamolaboratorio = _horafinalPrestamolaboratorio,
fklaboratorio = _fklaboratorio,
fkmaestro = _fkmaestro

where idPrestamolaboratorio = _idPrestamolaboratorio;

end;


create procedure p_eliminarPrestamoLab(in _idPrestamolaboratorio int(11) )
begin
delete from prestamo_laboratorio where idPrestamolaboratorio = _idPrestamolaboratorio;
end;
#end region
#region Cuentas_usuario
create procedure p_insertarUsuarios(in _idUsuario int, in _usuario varchar(200), in _contrase�a varchar(50),
in _nombre varchar(100), in _apellidos varchar(200), in _email varchar(50), in tipodecuenta varchar(100))
begin
insert into usuarios values(null ,_usuario,_contrase�a,_nombre,_apellidos,_email,_tipodecuenta);
end;

create procedure p_eliminarUsuarios(in _idUsuario int)
begin
delete from usuarios where idUsuario = _idUsuario;
end;

create procedure p_actualizarUsuarios(in _usuario varchar(200), in _contrase�a varchar(50),
in _nombre varchar(100), in _apellidos varchar(200), in _email varchar(50), in tipodecuenta varchar(100), in _idUsuario int)
begin
update usuarios set usuario = _usuario, contrase�a=_contrase�a, nombre = _nombre, apellidos = _apellidos, email = _email, tipodecuenta = _tipodecuenta where idUsuario =_idUsuario;
end;
#end region

#end region
#region Vistas
#region vistas de edificio
/*VISTAS DE EDIFICIO*/
create view v_Edificio as 
select IDEdificio as 'N�mero', NombreEdificio as 'Nombre Edificio'from Edificio;
#end region
#region vistas de categoria
/*VISTAS DE CATEGORIA*/
create view v_categoria as select categoria.nombreCategoria as "Nombre de la categoria", categoria.idCategoria as "Codigo" from categoria;
#end region
#region Vista Grupo
CREATE VIEW View_Grupo AS
SELECT idGrupo as 'ID Grupo', 
semestreGrupo as 'Semestre',
aulaGrupo as 'Aula',
carreraGrupo as 'Carrera' 
FROM grupo;
#end region
#region Vista Alumno
CREATE VIEW View_Alumno AS
SELECT numerocontrolAlumno as 'Numero control', 
nombreAlumno as 'Nombre',
apellidopaternoAlumno as 'Apellido paterno',
apellidomaternoAlumno as 'Apellido materno' ,
telefonoAlumno as 'Telefono',
correoAlumno as 'Correo',
codigocredencialAlumno as 'Codigo credencial',
fkgrupo as 'Grupo' from alumno,grupo where alumno.fkgrupo = grupo.idGrupo;
#end region
#region Vista de Laboratorios
create view v_laboratorio as 
select nombreLaboratorio as 'Nombre de laboratorio',
Edificio.NombreEdificio as 'Edificio',
color.nombreColor as 'Color' from Edificio,laboratorio,color where laboratorio.fkidEdificio = Edificio.IDEdificio and 
laboratorio.fkcolor = color.Idcolor;
#end region
#region Vista de materia
create view v_materia as
select claveMateria as 'Clave de materia',
nombreMateria as 'Nombre de materia' from materia;
#end region
#region Vista maestro
CREATE VIEW View_Maestros AS
SELECT claveMaestro as 'Clave Maestro', nombreMaestro as 'Nombre',apellidoPaternoMaestro as 'Apellido Paterno',
apellidoMaternoMaestro as 'Apellido Materno' FROM maestro;
#end region 
#region Vista color
CREATE VIEW View_Colores AS
SELECT idColor as 'Color', nombreColor as 'Nombre Color', significadoColor as 'Significado' FROM color;

#end region
#region Vista prestamoproyector
CREATE VIEW View_prestamoproyector AS
SELECT idPrestamoproyector as 'Proyector', 
fechaprestamoPrestamocable as 'Fecha del prestamo',
fechaentregaPrestamocable as 'Fecha de entrega',
horainicioProyector as 'Fecha de prestamo' ,
horafinalProyector as 'Fecha de entrega',
fkgrupo as 'Grupo' from alumno,grupo where alumno.fkgrupo = grupo.idGrupo;

#end region
#region Vista herramienta
CREATE VIEW v_herramienta AS
SELECT codigoHerramienta as 'Codigo de herramienta', 
conceptoHerramiento as 'Concepto de Herramiento',
etiquetaHerramienta as 'Etiqueta de la herramienta',
marcaHerramienta as 'Marca de la herramienta' ,
modeloHerramienta as 'Modelo de la herramienta',
estadoHerramienta as 'Estado de la herramienta',
ubicacionHerramienta as 'Ubicacion',
activoResguardoHerramienta as 'Resguardo',
plantaHerramienta as 'Edificio',
observacionesHerramienta as 'Observaciones',
fkEdificio as 'Edificio' from herramienta,Edificio where herramienta.fkEdificio = Edificio.IDEdificio,
fkcategoria as 'Categoria' from herramienta,categoria where herramienta.fkcategoria = categoria.idCategoria; 

#end region
#region vista de prestamo de cable
/* Vista de prestamo de cable*/
create view v_prestamocable as
select idPrestamocable as 'ID',
tipoPrestamocable as 'Tipo',
numerocables as 'numero',
fechaprestamoPrestamocable as 'fecha de prestamo',
fechaentregaPrestamocable as 'fecha de entrega',
fkgrupo as 'grupo' from prestamocable;
#end region
#region vista de materias_grupo
/*Vista de materias_Grupos*/
create view v_materiasgrupos as
select idMG as 'ID',
fkmateria as 'materia',
fkgrupo as 'grupo' from materias_grupos;
#end region
#region vista de almacenista
create view v_Almacenista as 
select idAlmacenista as 'ID',
nombreAlmacenista as 'Nombre',
apellidopaternoAlmacenista as 'Apellido Paterno',
apellidomaternoAlmacenista as 'Apellido Materno',
correoAlmacenista as 'Correo',
cargoAlmacenista as 'Cargo'
 from almacenista;
#end region
#region vista de Prestamo de laboratorio
CREATE VIEW view_PrestamoLaboratorio AS
SELECT PL.idPrestamolaboratorio as 'ID',
PL.fechaPrestamolaboratorio as 'Fecha Prestamo',
PL.horainicioPrestamolaboratorio as 'Hora inicio',
PL.horafinalPrestamolaboratorio as 'Hora final',
PL.fklaboratorio as 'Laboratorio', 
PL.fkmaestro as ' Clave Maestro'
FROM prestamo_laboratorio PL, maestro m, laboratorio lab
where PL.fklaboratorio = lab.nombreLaboratorio and PL.fkmaestro = m.nombreMaestro ;
#end region
#region vista de PrestamoHerramientas
/*VISTA DE PRESTAMOS DE HERRAMIENTAS*/
CREATE VIEW view_PrestamoHerramienta AS
SELECT PH.idprestamoHerramienta as 'ID',
PH.fechaPrestamoherrmienta as 'Fecha de Prestamo',
PH.fechadevolucionPrestamoherramienta as 'Fecha de devolucion',
PH.statusPrestamoherramienta as 'Estado',
PH.cantidadPrestamoherramienta as 'Cantidad',
PH.fkherramienta as 'Codigo de Herramienta', 
PH.fkalumno as 'Usuario',
PH.fkmateriaas 'Materia',
FROM prestamo_herramienta PH, materia m, herramienta h, alumno a
where PH.fkherramienta = h.codigoHerramienta and PH.fkalumno = a.nombreAlumno and
PH.fkmateria = m.nombreMateria;
#end region
#region vista de Usuarios
create view v_Usuarios as 
select idUsuario as 'ID',
usuario,
contrase�a,
nombre,
apellidos,
email,
tipodecuenta as 'Cuenta'
from usuarios;
#end region
#end region
#region Login#
/*Aqu� se anexa el script para acceso de usuario*/
#end region